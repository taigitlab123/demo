package com.company;

public class TypeCasting {
    public static void main(String[] args) {
        int myInt = 9;
        double myDouble = myInt;
        System.out.println(myInt);
        System.out.println(myDouble);


        //narrowCasting
        double Double = 10.76d;
        int sonuyen = (int) Double;
        System.out.println(Double);
        System.out.println(sonuyen);
    };

}

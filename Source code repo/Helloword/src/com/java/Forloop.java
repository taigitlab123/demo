package com.java;

public class Forloop {
    public static void main(String[] args ){
        for (int i = 0; i < 5; i++) {
            System.out.println(i);
        }
        //For-Each Loop
        String[] cars = {"Volvo", "BMW", "Ford", "Mazda"};
        for (String i : cars) {
            System.out.println(i);
        }
    }
}

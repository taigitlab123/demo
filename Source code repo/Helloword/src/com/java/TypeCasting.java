package com.java;

public class TypeCasting {
    public static void main(String[] args) {
    //    int myInt = 9;
//        double myDouble = myInt; // Automatic casting: int to double
//
//        System.out.println(myInt);
//        System.out.println(myDouble);
    double myDouble = 9.78d;
    int myInt = (int) myDouble; // Manual casting: double to int

        System.out.println(myDouble);
        System.out.println(myInt);
}
}

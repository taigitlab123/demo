package com.java;

public class Whileloop {
    public static void main(String[] args ){
        int i = 0;
        while (i < 5) {
            System.out.println(i);
            i++;
        }
//        // a variant of While loop
        int a = 0;
        do {
            System.out.println(a);
            a++;
        }
        while (a< 6);
    }
}

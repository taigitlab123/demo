package com.javatt;
public class ClassMethod {
    public void fullThrottle() {
        System.out.println("The car is going as fast as it can!");
    }
    public void speed(int maxSpeed) {
        System.out.println("Max speed is: " + maxSpeed);
    }
    public static void main(String[] args) {
        ClassMethod myCar = new ClassMethod();
        myCar.fullThrottle();
        myCar.speed(200);
    }
}




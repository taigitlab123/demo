package com.javatt;

public class Modifier {
    static void myStaticMethod() {
        System.out.println("DangThaiTai");
    }
    public void myPublicMethod() {
        System.out.println("DangThaiTai");
    }
    public static void main(String[ ] args) {
        myStaticMethod();
        Modifier myObj = new Modifier();
        myObj.myPublicMethod();
    }
}
